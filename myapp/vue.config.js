/*
 * @Author: 凌宇king
 * @Date: 2022-10-03 13:24:12
 * @LastEditTime: 2022-10-03 21:21:04
 */
const { defineConfig } = require('@vue/cli-service')
const AutoImport = require('unplugin-auto-import/webpack')
const Components = require('unplugin-vue-components/webpack')
const { ElementPlusResolver } = require('unplugin-vue-components/resolvers')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,
  // 配置public为绝对静态资源路径
  publicPath: './',
  // 设置css的主入口文件
  css: {
    loaderOptions: {
      scss: {
        additionalData: `@import '@/styles/main.scss';`
      }
    }
  },
  configureWebpack: {
    plugins: [
      AutoImport({
        resolvers: [ElementPlusResolver()],
      }),
      Components({
        resolvers: [ElementPlusResolver()],
      }),
    ],
  }
})
