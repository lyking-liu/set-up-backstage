/*
 * @Author: 凌宇king
 * @Date: 2022-10-03 19:11:08
 * @LastEditTime: 2022-10-04 16:19:07
 */
import server from '../utils/interceptors.js'

let http = (params) => {
    return new Promise((resolve, reject) => {
        server.request({
            method: 'get',
            ...params
        }).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}
export default http 