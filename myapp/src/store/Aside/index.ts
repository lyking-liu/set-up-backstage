/*
 * @Author: 凌宇king
 * @Date: 2022-10-04 19:23:25
 * @LastEditTime: 2022-10-04 19:31:51
 */
let aside: Object = {
    state: {
        Turn:true
    },
    getters: {
    },
    mutations: {
        POST_TURN(state:any) {
            state.Turn = !state.Turn
        }
    },
    actions: {
    },
    modules: {
    }
}
export default aside