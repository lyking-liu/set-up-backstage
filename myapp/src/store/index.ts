/*
 * @Author: 凌宇king
 * @Date: 2022-10-03 13:24:12
 * @LastEditTime: 2023-03-14 10:24:13
 */
import { createStore } from "vuex";
import aside from "./Aside/index";
export default createStore({
  state: {
    routers:{}
  },
  getters: {},
  mutations: {
    handleRouters(state:any,params:any) {
       state.routers = params
    }
  },
  actions: {},
  modules: {
    aside,
  },
});
