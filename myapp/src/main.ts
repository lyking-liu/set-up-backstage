/*
 * @Author: 凌宇king
 * @Date: 2022-10-03 13:24:12
 * @LastEditTime: 2023-03-02 14:32:44
 */
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'lib-flexible/flexible.js'
// 如果您正在使用CDN引入，请删除下面一行。
import * as ElementPlusIconsVue from "@element-plus/icons-vue";

const app = createApp(App);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}

createApp(App).use(store).use(router).mount('#app')
