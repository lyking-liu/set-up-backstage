/*
 * @Author: 凌宇king
 * @Date: 2022-10-04 17:40:30
 * @LastEditTime: 2022-10-04 17:41:24
 */
import md5 from 'js-md5'

function MD5(data) {
    return md5(data)
}
export default MD5