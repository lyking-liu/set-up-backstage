/*
 * @Author: 凌宇king
 * @Date: 2022-10-03 18:09:05
 * @LastEditTime: 2022-10-03 18:19:25
 */
function VaPass(data) {
    let ref = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/
    return !ref.test(data)
}
function VaUser(data) {
    let ref = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/
    return !ref.test(data)
}
export { VaPass, VaUser }