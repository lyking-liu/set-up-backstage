/*
 * @Author: 凌宇king
 * @Date: 2022-10-03 19:03:09
 * @LastEditTime: 2023-04-14 18:57:14
 */
import axios from 'axios'
import { ElMessage, ElLoading } from 'element-plus'
let baseurl = 'http://localhost:5030'


const server = axios.create({
    baseURL: baseurl,
})
// 添加请求拦截器
server.interceptors.request.use(function (config) {
    // console.log()
    window.loading = ElLoading.service({
        lock: true,
        text: 'Loading',
        background: 'rgba(0, 0, 0, 0.7)',
    })
    const userInfo = localStorage.getItem('userInfo')
    if (!location.hash.includes('login')) {
        if (!userInfo) {
            ElMessage({
                message: '用户信息已过期',
                type: 'warning',
            })
            setTimeout(() => {
                location.href = '#/login'
            }, 1000)
        }
    }
    // 在发送请求之前做些什么
    return config;
}, function (error) {
    loading.close()
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
server.interceptors.response.use(function (response) {
    loading.close()
    // 对响应数据做点什么
    return response;
}, function (error) {
    loading.close()
    // 对响应错误做点什么
    switch (error.response.status) {
        case 404: ElMessage({
            message: '当前请求路径错误',
            type: 'warning',
        })
            break
        case 500: ElMessage({
            message: '服务端错误',
            type: 'warning',
        })
            break
    }
    return Promise.reject(error);
});
export default server