/*
 * @Author: 凌宇king
 * @Date: 2022-10-03 13:24:12
 * @LastEditTime: 2023-05-15 20:48:11
 */
/*
 * @Author: 凌宇king
 * @Date: 2022-10-03 13:24:12
 * @LastEditTime: 2023-04-13 20:26:24
 */
/*
 * @Author: 凌宇king
 * @Date: 2022-10-03 13:24:12
 * @LastEditTime: 2023-03-02 15:02:32
 */
import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import store from '../store'
import axios from 'axios'
import {
  Avatar,
  HelpFilled,
  Grid,
  User,
  EditPen,
  Edit,
  Operation,
  Histogram,
  Discount,
  StarFilled,
  VideoCamera,
  Place,
  Guide
} from '@element-plus/icons-vue'
let Day = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日']
const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/Login/index.vue')
  },
  {
    path: '/404',
    name: '404',
    component: () => import('@/views/Home/childrens/404.vue')
  },
  {
    path: '/',
    redirect: '/404'
  },
  {
    name:'NoRouter',
    path: '/:id',
    redirect: '/404'
  }
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})
const cc = {
  path: '/home',
  name: 'home',
  component: () => import('@/views/Home/index.vue'),
  children: [
    {
      path: '/user',
      name: 'user',
      component: () => import('@/views/Home/childrens/UserOne.vue'),
      meta: {
        title: '用户信息',
        icon: User
      },
      children: [
        {
          path: '/userupdate',
          name: 'userupdate',
          component: () => import('@/views/Home/childrens/UserUpdate.vue'),
          meta: {
            catorage: '用户信息',
            title: '用户信息更新',
            icon: EditPen
          }
        },
        {
          path: '/userlist',
          name: 'userlist',
          component: () => import('@/views/Home/childrens/UserList.vue'),
          meta: {
            catorage: '用户信息',
            title: '用户信息列表',
            icon: Operation
          }
        }
      ]
    },
    {
      path: '/adminator',
      name: 'adminator',
      component: () => import('@/views/Home/childrens/AdminatorOne.vue'),
      meta: {
        title: '管理员信息',
        icon: Avatar
      },
      children: [
        {
          path: '/adminatorupdate',
          name: 'adminatorupdate',
          component: () => import('@/views/Home/childrens/AdminatorUpdate.vue'),
          meta: {
            catorage: '管理员信息',
            title: '管理员信息更新',
            icon: Edit
          }
        },
        {
          path: '/adminatorlist',
          name: 'adminatorlist',
          component: () => import('@/views/Home/childrens/AdminatorList.vue'),
          meta: {
            catorage: '管理员信息',
            title: '管理员信息列表',
            icon: Operation
          }
        }
      ]
    },
    {
      path: '/orderbuy',
      name: 'orderbuy',
      component: () => import('@/views/Home/childrens/OrderBuyOne.vue'),
      meta: {
        title: '订单信息',
        icon: Grid
      },
      children: [
        {
          path: '/orderbuyupdate',
          name: 'orderbuyupdate',
          component: () => import('@/views/Home/childrens/OrderBuyUpdate.vue'),
          meta: {
            catorage: '订单信息',
            title: '订单信息更新',
            icon: Edit
          }
        },
        {
          path: '/orderbuylist',
          name: 'orderbuylist',
          component: () => import('@/views/Home/childrens/OrderBuyList.vue'),
          meta: {
            catorage: '订单信息',
            title: '订单信息列表',
            icon: Operation
          }
        }
      ]
    },
    {
      path: '/city',
      name: 'city',
      component: () => import('@/views/Home/childrens/CityOne.vue'),
      meta: {
        title: '城市信息',
        icon: Discount
      },
      children: [
        {
          path: '/cityupdate',
          name: 'cityupdate',
          component: () => import('@/views/Home/childrens/CityUpdate.vue'),
          meta: {
            catorage: '城市信息',
            title: '城市信息更新',
            icon: Edit
          }
        },
        {
          path: '/citylist',
          name: 'citylist',
          component: () => import('@/views/Home/childrens/CityList.vue'),
          meta: {
            catorage: '城市信息',
            title: '城市信息列表',
            icon: Operation
          }
        }
      ]
    },
    {
      path: '/movie',
      name: 'movie',
      component: () => import('@/views/Home/childrens/MovieOne.vue'),
      meta: {
        title: '电影信息',
        icon: StarFilled
      },
      children: [
        {
          path: '/movieupdate',
          name: 'movieupdate',
          component: () => import('@/views/Home/childrens/MovieUpdate.vue'),
          meta: {
            catorage: '电影信息',
            title: '电影信息更新',
            icon: Edit
          }
        },
        {
          path: '/movielist',
          name: 'movielist',
          component: () => import('@/views/Home/childrens/MovieList.vue'),
          meta: {
            catorage: '电影信息',
            title: '电影信息列表',
            icon: Operation
          }
        }
      ]
    },
    {
      path: '/cinemas',
      name: 'cinemas',
      component: () => import('@/views/Home/childrens/CinemasOne.vue'),
      meta: {
        title: '影院信息',
        icon: VideoCamera
      },
      children: [
        {
          path: '/cinemasupdata',
          name: 'cinemasupdata',
          component: () => import('@/views/Home/childrens/CinemasUpdata.vue'),
          meta: {
            catorage: '影院信息',
            title: '影院信息更新',
            icon: Edit
          }
        },
        {
          path: '/cinemaslist',
          name: 'cinemaslist',
          component: () => import('@/views/Home/childrens/CinemasList.vue'),
          meta: {
            catorage: '影院信息',
            title: '影院信息列表',
            icon: Operation
          }
        }
      ]
    },
    {
      path: '/authorityrole',
      name: 'authorityrole',
      component: () => import('@/views/Home/childrens/AuthorityRole.vue'),
      meta: {
        title: '角色分配',
        icon: Place
      }
    },
    {
      path: '/authorityupdate',
      name: 'athorityupdate',
      component: () => import('@/views/Home/childrens/AuthorityUpdate.vue'),
      meta: {
        title: '权限分配',
        icon: Guide
      }
    },
    {
      path: '/pay',
      name: 'pay',
      component: () => import('@/views/Home/childrens/PayList.vue'),
      meta: {
        title: '用户支付',
        icon: HelpFilled
      }
    },
    {
      path: '/echars',
      name: 'echars',
      component: () => import('@/views/Home/childrens/EchartsVIew.vue'),
      meta: {
        title: '可视化数据',
        icon: Histogram
      }
    }
  ]
}
if (localStorage.getItem('userInfo')) {
  // 访问量
  axios({
    url: "http://localhost:5032/vistor?role=" + localStorage.getItem('role'),
  }).then(res => {
    let dateValue = res.data[0].num.find((item: any) => item.date === Day[new Date().getDay() - 1])
    dateValue.value += 1
    axios({
      url: "http://localhost:5032/vistor/" + res.data[0].id,
      method: 'PATCH',
      data: {
        num: res.data[0].num
      }
    })
  })
  router.removeRoute('NoRouter')
  axios({
    url: `http://localhost:5031/register?username=${
      JSON.parse(localStorage.getItem('userInfo') || "{username:'undefined'}")
        .username
    }`
  }).then((resR: any) => {
    resR = resR.data[0].role
    axios({
      url: 'http://localhost:5030/role?name=' + resR
    }).then((res: any) => {
      let newRouter: any = []
      res.data[0].author.children
        .filter((item: any) => item.show === true)
        .forEach((itemF: any) => {
          let newArr = cc.children.find(
            (items: any) => items.name == itemF.name
          )
          newRouter.push(newArr)
        })
      cc.children = newRouter
      store.commit('handleRouters', cc)
      localStorage.setItem('routerRole', JSON.stringify(cc))
      router.addRoute(cc)
      router.push(router.currentRoute.value.fullPath)
    })
  })
}
export default router
